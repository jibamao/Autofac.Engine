#Autofac.Engine
应用方式请查看Lc.Autofac.Engine.Tests测试库
    EngineContext.Initialize(false, ScopeTag.Http);     初始化
    DependencyRegistrar                                 注册
    EngineContext.Resolve<T>()                          实例化

Code : {
    Init:  EngineContext.Initialize(false, ScopeTag.Http);
    WebMvc SetResolver:  DependencyResolver.SetResolver(new AutofacDependencyResolver(EngineContext.Scope));
    WebApi SetResolver:  GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(EngineContext.Scope);
    Registrar:  IDependencyRegistrar:  { builder.RegisterType<Service>().As<IService>().InstancePerLifetimeScope(); }
    Resolve: EngineContext.Resolve<IService>();
}

##目录
<pre>
|---README.MD：                        相关说明
|---Autofac.Engine/：                  .Net解决方案文件夹
    |---Autofac.Engine                 Autofac.Engine项目解决方案文件夹
        |---Autofac.Engine.csproj      Autofac.Engine项目文件
    |---Tests                          测试解决方案文件夹
        |---Lc.Autofac.Engine.Tests    项目测试库
        |---Lc.Tests                   NUnit测试库
</pre>